﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Nauta
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemPage : ContentPage
	{
        public string article;

		public ItemPage (string data, string articleID)
		{
			InitializeComponent ();

            this.Logo.HeightRequest = Home.GetResponsiveSize(100);
            this.itemTitle.FontSize = Home.GetResponsiveSize(18);

            ReadJson(data, articleID);
		}

        private async void ReadJson(string json, string articleID)
        {
            try
            {
                Newtonsoft.Json.Linq.JObject o = Newtonsoft.Json.Linq.JObject.Parse(json);

                article = articleID;

                foreach (Newtonsoft.Json.Linq.JProperty property in o.Properties())
                {
                    if (property.Name.Equals("msg"))
                    {
                        await DisplayAlert("Error", property.Value.ToString(), "OK");
                        //Scanner.scaneFinish = false;
                        await Navigation.PopToRootAsync();
                        return;
                    }

                    if (property.Name.Equals("nombre"))
                    {
                        this.itemTitle.Text = (string)property.Value;
                        continue;
                    }

                    if (property.Name.Equals("imagen"))
                    {
                        this.itemImage.Source = new UriImageSource()
                        {
                            Uri = new Uri("http://" + property.Value.ToString()),
                            CachingEnabled = false
                        };

                        continue;
                    }

                    if (property.Name.Equals("observaciones") && !property.Value.ToString().Equals(""))
                    {
                        this.itemObservation.Text = (string)property.Value;
                        continue;
                    }

                    if (property.Name.Equals("nombre"))
                    {
                        this.itemTitle.Text = (string)property.Value;
                        this.itemTitle.Text = this.itemTitle.Text.ToUpper();
                        continue;
                    }

                    if (property.Name.Equals("success") || property.Name.Equals("id_articulo") || property.Name.Equals("id_Cer") || property.Name.Equals("artes") || property.Name.Equals("ilustraciones"))
                    {
                        continue;
                    }

                    if (property.Value.ToString().Equals("") || property.Value.Equals(string.Empty) || property.Value.Equals(null) || property.Value.Equals("null"))
                    {
                        continue;
                    }

                    string propertyAux = property.Name;
                    string valueAux = (string) property.Value;

                    if (propertyAux.Equals("fecha_Publicacion"))
                    {
                        propertyAux = "fecha_Publicacion_del_ejemplar";

                        string[] splitDate = valueAux.Split('-');

                        valueAux = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
                    }

                    if (propertyAux.Equals("anio"))
                    {
                        propertyAux = "anio_editorial";

                        string[] splitDate = valueAux.Split('-');

                        valueAux = splitDate[0];
                    }

                    if (propertyAux.Equals("fecha_Certificacion"))
                    {
                        propertyAux = "fecha_Certificacion_del_articulo";

                        string[] splitCompleteDate = valueAux.Split(' ');
                        string[] splitDate = splitCompleteDate[0].Split('-');

                        valueAux = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                    }

                    AddRow(propertyAux, valueAux);
                    
                }
            }
            catch(JsonReaderException e)
            {
                await DisplayAlert("ERROR","Código QR no valido", "Continuar");
                //Scanner.scaneFinish = false;
                await Navigation.PopToRootAsync();
                return;
            } 
        }

        private void AddRow(string property, string value)
        {
            StackLayout cell = new StackLayout();

            StackLayout itemLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
            };

            cell.Children.Add(itemLayout);

            Label itemProperty = new Label
            {
                TextColor = Color.Black,
                FontFamily = "sans serif",
                Text = CleanName(property),
                HorizontalOptions = LayoutOptions.StartAndExpand,
            };

            Label itemValue = new Label
            {
                TextColor = Color.Black,
                FontFamily = "sans serif",
                Text = UppercaseFirst(value),
                HorizontalOptions = LayoutOptions.EndAndExpand,
                HorizontalTextAlignment = TextAlignment.End,
            };

            if (property.Equals("anio"))
            {
                itemValue.Text = value.Split('-')[0];
            }

            itemLayout.Children.Add(itemProperty);
            itemLayout.Children.Add(itemValue);

            BoxView separation = new BoxView
            {
                BackgroundColor = Color.Gray,
                HeightRequest = 1,
            };

            cell.Children.Add(separation);

            this.itemTable.Children.Add(cell);
        }

        private string CleanName(string value)
        {
            string result = UppercaseFirst(value.ToLower());

            result = result.Replace("_", " ");

            result = AutoCorrector(result);

            return result;
        }

        private string AutoCorrector(string input)
        {
            string[] aux = input.Split(' ');
            string result = string.Empty;

            for(int i = 0; i < aux.Length; i++)
            {
                if (aux[i].Equals("Anio"))
                {
                    aux[i] = "Año";
                }

                if (aux[i].Equals("certificacion"))
                {
                    aux[i] = "certificación";
                }

                if (aux[i].Equals("Numero"))
                {
                    aux[i] = "Número";
                }

                if (aux[i].Equals("publicacion"))
                {
                    aux[i] = "publicación";
                }

                if (aux[i].Equals("Genero"))
                {
                    aux[i] = "Género";
                }

                if (aux[i].Equals("Edicion"))
                {
                    aux[i] = "Edición";
                }

                if (aux[i].Equals("edicion"))
                {
                    aux[i] = "edición";
                }

                if (aux[i].Equals("Articulo"))
                {
                    aux[i] = "Artículo";
                }

                if (aux[i].Equals("articulo"))
                {
                    aux[i] = "artículo";

                }
                if (aux[i].Equals("paginas"))
                {
                    aux[i] = "páginas";
                }

                result += aux[i] + " ";
            }

            

            return result;
        }

        private string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            return char.ToUpper(s[0]) + s.Substring(1);
        }

        private void SeeMore(object sender, EventArgs e)
        {
            Client.OpenWebPage("http://interactive.utalca.cl/prjcomics/prjcomics/view/product-mobile/index.php?id=" + article);
        }

        private async void Back(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        private async void Back()
        {
            await Navigation.PopToRootAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            Back();
            return true;
        }

    }
}