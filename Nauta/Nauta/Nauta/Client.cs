﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using Xamarin.Forms;

namespace Nauta
{
    public static class Client
    {
        private static string result = "Leyendo...";
        private static bool finish;

        public async static void GetRequest(string url)
        {
            finish = false;

            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetAsync(url);
                var json = await response.Content.ReadAsStringAsync();
                result = json;
                //result = JsonConvert.SerializeObject(json);
            }
            catch (Exception ex)
            {

                result = ex.GetType().FullName + "\n";
                result += ex.GetBaseException().ToString();
            }

            finish = true;
           
        }

        public static string GetResult()
        {
            return result;
        }

        public static bool IsFinish()
        {
            return finish;
        }

        public static void OpenWebPage(string url)
        {
            Device.OpenUri(new Uri(url));
        }
    }
}
