﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;
using ZXing.Net.Mobile;
using Lottie.Forms;
using Android.Net;

namespace Nauta
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Home : ContentPage
	{
        public static bool scannerReady = false;

        public Home ()
		{
			InitializeComponent ();
            ShowPage();
        }

        void ShowPage()
        {
            DisplayAlert("widht", this.Width.ToString(), "OK");

            var main = new StackLayout
            {
                
            };

            var icon = new Image
            {
                Source = "LOGO.png",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand,
                WidthRequest = GetResponsiveSize(70)
            };

            var instructions = new Label
            {
                HorizontalOptions = LayoutOptions.Center,
                TextColor = Color.Black,
                FontFamily = "sans serif",
                FontSize = GetResponsiveSize(16),
                HorizontalTextAlignment = TextAlignment.Center,
                Text = "Presiona el botón para comenzar la lectura",
                Margin = new Thickness(0, GetResponsiveSize(21), 0,0)
            };

            var animation = new AnimationView
            {
                Animation = "radar.json",
                AutoPlay = true,
                Scale = GetResponsiveSize(0.8f),
                Loop = true,
                HeightRequest = GetResponsiveSize(200),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

            var grid = new Grid
            {
                RowSpacing = 0,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center
            };

            var scanner = new Button
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Opacity = 0
            };

            scanner.Clicked += OnClickScan;

            grid.Children.Add(animation);
            grid.Children.Add(scanner);

            var store = new Button
            {
                Text = "IR A LA TIENDA",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("#4E5FC2"),
                FontFamily = "sans serif",
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand,
                HeightRequest = 70,
            };

            store.Clicked += ToStore;

            main.Children.Add(icon);
            main.Children.Add(instructions);
            main.Children.Add(grid);
            main.Children.Add(store);

            main.Padding = GetPagePadding();

            main.Spacing = 0;

            Content = main;
        }

        private static Thickness GetPagePadding()
        {
            double topPadding;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    topPadding = 70;
                    break;
                default:
                    topPadding = 50;
                    break;
            }

            return new Thickness(50, topPadding, 50, 50);
        }

        public void OnClickScan(object sender, EventArgs e)
        {
            OpenScaner();
        }

        public async void OpenScaner()
        {
            scannerReady = true;

            var scanPage = new ZXingScannerPage();

            var customOverlay = new StackLayout
            {
                // HorizontalOptions = LayoutOptions.FillAndExpand,
                // VerticalOptions = LayoutOptions.FillAndExpand
            };

            var instructions = new Label
            {
                Text = "Posiciona el escáner frente al código QR y mantén hasta que la aplicación finalice la lectura.",
                TextColor = Color.White,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                FontSize = GetResponsiveSize(18),
                HorizontalOptions = LayoutOptions.Center
            };

            var stackUp = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand,
                BackgroundColor = Color.Black,
                HeightRequest = GetResponsiveSize(100),
                Spacing = 0,
                Opacity = 0.75
            };

            var stackDown = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand,
                BackgroundColor = Color.Black,
                HeightRequest = GetResponsiveSize(100),
                Spacing = 0,
                Opacity = 0.75
            };

            stackUp.Children.Add(instructions);

            customOverlay.Children.Add(stackUp);
            customOverlay.Children.Add(stackDown);
            // Pass in the custom overlay
            scanPage = new ZXingScannerPage(customOverlay: customOverlay);
            scanPage.OnScanResult += (result) => {
                scanPage.IsScanning = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    //Navigation.PopAsync();
                    FinishScanner(result.Text);
                    //DisplayAlert("Scanned Barcode", result.Text, "OK");
                });
            };

            await Navigation.PushAsync(scanPage, true);
        }

        private async void FinishScanner(string result)
        {
            if (!scannerReady)
                return;

            scannerReady = false;

            string link = "http://interactive.utalca.cl/prjcomics/prjcomics/controller/articulos/getInfoMobile?qr=";
            link += result;

            Client.GetRequest(link);

            while (!Client.IsFinish())
            {
                await Task.Delay(1000);
            }

            await Navigation.PushAsync(new ItemPage(Client.GetResult(), result), true);
        }

        public static void ToStore(object sender, EventArgs e)
        {
            Client.OpenWebPage("http://interactive.utalca.cl/prjcomics/prjcomics/");
        }

        public static float GetResponsiveSize(float minSize)
        {
           return (App.ScreenWidth * minSize) / 320;
        }
    }
}