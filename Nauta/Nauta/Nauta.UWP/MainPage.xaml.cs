﻿namespace Nauta.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            LoadApplication(new Nauta.App());
        }
    }
}